sudo adduser iis

sudo -u postgres createuser iis

sudo -u postgres createdb iis

sudo -u postgres psql
#появится prompt psql:

alter user iis with encrypted password 'iis';

GRANT ALL PRIVILEGES ON DATABASE iis TO iis;

\connect iis
#создаём таблицы

create table department(
  id bigserial primary key,
  DepCode varchar(20),
  DepJob varchar(100),
  Description varchar(255),
 CONSTRAINT unique_code_job UNIQUE (DepCode, DepJob)
);


INSERT INTO department (DepCode, DepJob, Description)
    VALUES ('dep code 1', ' some job 1', ' some description 1' );

INSERT INTO department (DepCode, DepJob, Description)
    VALUES ('dep code 2', ' some job 2', ' some description 2');


INSERT INTO department (DepCode, DepJob, Description)
    VALUES ('dep code 3', ' some job 3', ' some description 3');

GRANT USAGE, SELECT ON SEQUENCE department_id_seq TO iis;

#покидаем psql
\q

Проверяем коннект:
psql -h 127.0.0.1 -p 5432 -U iis -d iis

теперь следует проверить настройки соединения в resources/connection.properties и можно запускать приложение


