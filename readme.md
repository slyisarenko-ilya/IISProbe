Реализация тестового задания 

Создать java приложение, выполняющее две основные функции:
1) выгрузка содержимого таблицы БД в XML файл;
2) синхронизация содержимого таблицы БД по заданному XML файлу.
...
(полностью текст задания в файле постановка-задачи.txt в корне проекта)

Скрипт создания пользователя, базы postgresql (для Ubuntu-подобных систем linux)
в файле create.sh

Конфигурация БД: /src/main/resources/connection.properties

Запуск:

для linux: 
xmltodb.sh или dbtoxml.sh 

для windows: 
xmltodb.bat или dbtoxml.bat

либо при помощи maven: 

из xml в базу:
mvn compile exec:java -Dexec.args="sync DepartmentsBucket.xml"

из базы в xml:
mvn compile exec:java -Dexec.args="DepartmentsBucket.xml"

