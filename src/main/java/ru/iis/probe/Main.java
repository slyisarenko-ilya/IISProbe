package ru.iis.probe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iis.probe.process.Process;
import ru.iis.probe.process.ProcessFactory;
import ru.iis.probe.process.ProgramArguments;

import java.util.Arrays;

class Main {
    private static final Logger logger =  LoggerFactory.getLogger(Main.class);

    public static void main(String[] args){
        //если в параметрах указано dbsync - пишем из xml в базу
        //если же только имя файла xml - тогда из базы в xml
        logger.info("Args: " + String.valueOf(Arrays.asList(args)));

        ProgramArguments programArguments = new ProgramArguments(args);
        Process process = new ProcessFactory().createProcess(programArguments.getOperation(), programArguments.getFileName());
        process.execute();
    }

}
