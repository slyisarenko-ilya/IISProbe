package ru.iis.probe.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iis.probe.exception.IISProbeException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectService {
    private static final Logger logger =  LoggerFactory.getLogger(ConnectionPropertiesProvider.class);

    private final ConnectionPropertiesProvider propertiesProvider;

    public ConnectService(ConnectionPropertiesProvider propertiesProvider){
        this.propertiesProvider = propertiesProvider;
    }

    public Connection getConnection(){
        try {
            String driverClassName = propertiesProvider.getDriverName();
            Class.forName(driverClassName);
            logger.info(String.format("Database driver name %s ", driverClassName));
            String connectionString = String.format("%s://%s:%s/%s",
                    propertiesProvider.getProtocol(),
                    propertiesProvider.getHostName(),
                    propertiesProvider.getPort(),
                    propertiesProvider.getDatabaseName());

            String username = propertiesProvider.getUserName();
            String password = propertiesProvider.getPassword();
            logger.info(String.format("Database connect url: %s, username: %s, password: %s ", connectionString, username, password));

            return DriverManager.getConnection(
                     connectionString,username, password);
        } catch (ClassNotFoundException | SQLException e) {
            throw new IISProbeException(e);
        }
    }
}
