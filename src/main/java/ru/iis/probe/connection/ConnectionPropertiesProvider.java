package ru.iis.probe.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iis.probe.exception.IISProbeException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConnectionPropertiesProvider {
    private static final Logger logger =  LoggerFactory.getLogger(ConnectionPropertiesProvider.class);

    private final Properties properties;

    private final static String DRIVER_NAME = "driverName";
    private final static String PROTOCOL = "protocol";
    private final static String HOST_NAME = "hostName";
    private final static String PORT = "port";
    private final static String DATABASE_NAME = "databaseName";
    private final static String USER_NAME = "userName";
    private final static String PASSWORD = "password";


    public ConnectionPropertiesProvider(String propertiesFileName){
        properties = load(propertiesFileName);
    }

    private Properties load(String propertiesFileName){
        try {
            InputStream inputStream = getClass().getClassLoader()
                    .getResourceAsStream(propertiesFileName);
            logger.info(String.format("Load properties from file %s", propertiesFileName));
            Properties properties = new Properties();
            properties.load(inputStream);
            return properties;
        } catch (IOException e) {
            throw new IISProbeException(e);
        }

    }

    String getDriverName(){
        return properties.getProperty(DRIVER_NAME);
    }

    String getProtocol(){
        return properties.getProperty(PROTOCOL);
    }

    String getHostName(){
        return properties.getProperty(HOST_NAME);
    }

    String getPort(){
        return properties.getProperty(PORT);
    }

    String getDatabaseName(){
        return properties.getProperty(DATABASE_NAME);
    }

    String getUserName(){
        return properties.getProperty(USER_NAME);
    }

    String getPassword(){
        return properties.getProperty(PASSWORD);
    }

}
