package ru.iis.probe.entity;

import java.util.Objects;

class AllFieldsDepartmentCompareStrategy implements CompareStrategy<Department> {

    @Override
    public boolean equals(Department object1, Department object2) {
        return Objects.equals(object1.getDepCode(), object2.getDepCode()) &&
                Objects.equals(object1.getDepJob(), object2.getDepJob()) &&
                Objects.equals(object1.getDescription(), object2.getDescription());
    }

    @Override
    public int hashCode(Department object) {
        return Objects.hash(object.getDepCode(), object.getDepJob(), object.getDescription());
    }

}
