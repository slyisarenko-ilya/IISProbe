package ru.iis.probe.entity;

public interface CompareStrategy<T> {
    boolean equals(T object1, T object2);
    int hashCode(T object);
}
