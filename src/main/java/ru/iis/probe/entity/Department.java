package ru.iis.probe.entity;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Optional;

@XmlAccessorType(XmlAccessType.FIELD)
public class Department {

    @XmlTransient()
    private  Integer id;

    @XmlElement(name = "DepCode")
    private String depCode;

    @XmlElement(name = "DepJob")
    private  String depJob;

    @XmlElement(name = "Description")
    private  String description;

    /* Механизм сравнения элементов внутри hash-структур, например, HashMap */
    private static CompareStrategy<Department> compareStrategy;

    public Department() {
        compareStrategy = new AllFieldsDepartmentCompareStrategy();
    }


    public Department(Integer id, String depCode, String depJob, String description) {
        this();
        this.id = id;
        this.depCode = depCode;
        this.depJob = depJob;
        this.description = description;
    }


    public Department(String depCode, String depJob, String description) {
        this(null, depCode, depJob, description);
    }

    public String getDepCode() {
        return depCode;
    }

    public void setDepCode(String depCode) {
        this.depCode = depCode;
    }

    public String getDepJob() {
        return depJob;
    }

    public void setDepJob(String depJob) {
        this.depJob = depJob;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean equalByNaturalKey(Department other){
        return Optional.ofNullable(other).map(
                dep->this.getDepCode().equals(dep.getDepCode()) && this.getDepJob().equals(dep.getDepJob())
        ).orElse(false);
    }


    @Override
    public String toString() {
        return "Department{" +
                "depCode='" + depCode + '\'' +
                ", depJob='" + depJob + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return compareStrategy.equals(this, that);
    }


    @Override
    public int hashCode() {
        return compareStrategy.hashCode(this);
    }

    public static void setCompareStrategy(CompareStrategy<Department> strategy) {
        compareStrategy = strategy;
    }

    public static CompareStrategy<Department> getCompareStrategy() {
        return compareStrategy;
    }

}
