package ru.iis.probe.entity;

import ru.iis.probe.entity.Department;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "DepartmentBucket")
@XmlAccessorType(XmlAccessType.FIELD)
public class DepartmentsBucket {

    @XmlElementWrapper(name = "departments")
    private List<Department> data;

    public List<Department> getData() {
        return data;
    }

    public void setData(List<Department> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "DepartmentsBucket{" +
                "data=" + data +
                '}';
    }
}
