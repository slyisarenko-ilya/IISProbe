package ru.iis.probe.exception;

public class IISProbeException extends RuntimeException {
    public IISProbeException(String message){
        super(message);
    }

    public IISProbeException(Throwable e){
        super(e);
    }
}
