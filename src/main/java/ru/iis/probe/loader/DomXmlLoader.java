package ru.iis.probe.loader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ru.iis.probe.entity.Department;
import ru.iis.probe.entity.DepartmentsBucket;
import ru.iis.probe.exception.IISProbeException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayList;
import java.util.List;

public class DomXmlLoader implements  Provider<DepartmentsBucket> {

    private static final Logger logger = LoggerFactory.getLogger(DomXmlLoader.class);

    private final String fileName;
    private final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    public DomXmlLoader(String fileName){
        this.fileName = fileName;
    }

    @Override
    public DepartmentsBucket load() {

            try {
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document document = builder.parse(fileName);

                document.getDocumentElement().normalize();

                if (!document.getDocumentElement().getNodeName().equals("DepartmentBucket")) {
                    throw new IISProbeException( "Root must be DepartmentBucket");
                }

                NodeList nodeList = document.getElementsByTagName("data");
                List<Department> data = new ArrayList<>();

                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node node = nodeList.item(i);
                    if (Node.ELEMENT_NODE == node.getNodeType()) {
                        Element element = (Element) node;
                        String depCode = element.getElementsByTagName("DepCode").item(0).getTextContent();
                        String depJob = element.getElementsByTagName("DepJob").item(0).getTextContent();
                        String description = element.getElementsByTagName("Description").item(0).getTextContent();

                        Department department = new Department(depCode, depJob, description);
                        data.add(department);

                        logger.info("Read department from xml: " + department);
                    }
                }
                DepartmentsBucket bucket = new DepartmentsBucket();
                bucket.setData(data);
                return bucket;
            } catch (Exception e) {
                throw new IISProbeException(e);
            }
    }
}
