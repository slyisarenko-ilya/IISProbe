package ru.iis.probe.loader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iis.probe.entity.DepartmentsBucket;
import ru.iis.probe.exception.IISProbeException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class JaxbXmlLoader implements Provider<DepartmentsBucket> {
    private static final Logger logger =  LoggerFactory.getLogger(JaxbXmlLoader.class);

    private final String fileName;

    public JaxbXmlLoader(String fileName){
        this.fileName = fileName;
    }

    @Override
    public DepartmentsBucket load() {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(DepartmentsBucket.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            File file = new File(fileName);

            DepartmentsBucket departmentsBucket = (DepartmentsBucket) jaxbUnmarshaller.unmarshal(file);
            logger.info(String.format("Loaded from %s, entity: %s", fileName, departmentsBucket));

            return departmentsBucket;

        } catch (JAXBException e) {
            throw new IISProbeException(e);
        }
    }
}
