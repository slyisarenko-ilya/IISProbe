package ru.iis.probe.loader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iis.probe.connection.ConnectService;
import ru.iis.probe.entity.DepartmentsBucket;
import ru.iis.probe.exception.IISProbeException;
import ru.iis.probe.entity.Department;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class PgSqlLoader implements Provider<DepartmentsBucket> {
    private static final Logger logger =  LoggerFactory.getLogger(PgSqlLoader.class);

    private final ConnectService connectService;
    private final static String selectDepartmentsSql = "select DepCode, DepJob, Description FROM department";

    public PgSqlLoader(ConnectService connectService){
        this.connectService = connectService;
    }

    public DepartmentsBucket load() {
        try(Connection conn = connectService.getConnection()) {
            try {
                List<Department> data = query(conn);
                DepartmentsBucket departmentBucket  = new DepartmentsBucket();
                departmentBucket.setData(data);
                return departmentBucket;
            } catch (SQLException e) {
                throw new IISProbeException(e);
            }
        } catch (SQLException e) {
            throw new IISProbeException(e);
        }
    }

    private static List<Department> query(Connection connection) throws SQLException {

        List<Department> results;
        try(Statement stmt = connection.createStatement()){

            try(ResultSet rs = stmt.executeQuery(selectDepartmentsSql)){
                results = list(rs);
            }

        }
        return results;
    }

    private static List<Department> list(ResultSet rs) throws SQLException {
        List<Department> results = new ArrayList<>();
        while (rs.next()) {
            Department department = new Department();
            department.setDepCode(rs.getString("DepCode"));
            department.setDepJob(rs.getString("DepJob"));
            department.setDescription(rs.getString("Description"));
            logger.info(String.format("Row from database %s", department));

            results.add(department);
        }
        return results;
    }
}
