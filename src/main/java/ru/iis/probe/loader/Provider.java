package ru.iis.probe.loader;

public interface Provider<T> {
    T load();
}
