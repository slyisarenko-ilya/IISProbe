package ru.iis.probe.process;

import ru.iis.probe.loader.Provider;
import ru.iis.probe.validation.SourceValidator;
import ru.iis.probe.writer.Writer;

class DataTransport<T>{

    private SourceValidator<T> validator;

    void setValidator(SourceValidator<T> validator){
        this.validator = validator;
    }

    void send(Provider<T> provider, Writer<T> writer){
        T entity = provider.load();
        if(validator != null){
            validator.validate(entity);
        }
        writer.write(entity);
    }
}
