package ru.iis.probe.process;

import ru.iis.probe.connection.ConnectService;
import ru.iis.probe.connection.ConnectionPropertiesProvider;
import ru.iis.probe.entity.DepartmentsBucket;
import ru.iis.probe.loader.PgSqlLoader;
import ru.iis.probe.loader.Provider;
import ru.iis.probe.validation.DepartmentBucketValidator;
import ru.iis.probe.writer.JaxbXmlWriter;
import ru.iis.probe.writer.Writer;

public class DatabaseToXmlProcess implements Process{
    private final String fileName;

    DatabaseToXmlProcess(String fileName){
        this.fileName = fileName;
    }

    @Override
    public void execute() {
        ConnectionPropertiesProvider connectionPropertiesProvider = new ConnectionPropertiesProvider("connection.properties");
        ConnectService connectService = new ConnectService(connectionPropertiesProvider);
        Writer<DepartmentsBucket> xmlWriter = new JaxbXmlWriter(fileName);
        DataTransport<DepartmentsBucket> transport = new DataTransport<>();
        Provider<DepartmentsBucket> dbProvider = new PgSqlLoader(connectService);
        transport.setValidator(new DepartmentBucketValidator());
        transport.send(dbProvider, xmlWriter);
    }
}
