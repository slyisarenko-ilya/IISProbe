package ru.iis.probe.process;

public interface Process {
    void execute();
}
