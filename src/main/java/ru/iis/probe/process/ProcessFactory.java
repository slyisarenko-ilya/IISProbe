package ru.iis.probe.process;

import ru.iis.probe.exception.IISProbeException;

public class ProcessFactory {
    public Process createProcess(ProgramArguments.Operation operation, String fileName){
        switch (operation){
            case XML_TO_DATABASE:
                return new XmlToDatabaseProcess(fileName);
            case DATABASE_TO_XML:
                return new DatabaseToXmlProcess(fileName);
        }
        throw new IISProbeException("Неизвестная операция " + operation);

    }
}
