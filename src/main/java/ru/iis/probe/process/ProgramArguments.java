package ru.iis.probe.process;

import ru.iis.probe.exception.IISProbeException;

public class ProgramArguments {
    enum Operation{
        XML_TO_DATABASE,
        DATABASE_TO_XML
    }

    private Operation operation;
    private String fileName;

    public ProgramArguments(String[] args){
        if(args.length == 1){
            fileName = args[0];
            operation = Operation.DATABASE_TO_XML;
        } else if (args.length == 2){
            String op = args[0];
            if("sync".equalsIgnoreCase(op)){
                this.operation = Operation.XML_TO_DATABASE;
            } else{
                throw new IISProbeException("Допустимо использование sync в качестве команды");
            }
            fileName = args[1];
        } else{
            throw new IISProbeException("Допустимые аргументы: [sync] имя_файла.xml");
        }
    }

    public String getFileName() {
        return fileName;
    }

    public Operation getOperation() {
        return operation;
    }

}
