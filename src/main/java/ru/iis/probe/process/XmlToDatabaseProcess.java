package ru.iis.probe.process;

import ru.iis.probe.connection.ConnectService;
import ru.iis.probe.connection.ConnectionPropertiesProvider;
import ru.iis.probe.entity.DepartmentsBucket;
import ru.iis.probe.loader.PgSqlLoader;
import ru.iis.probe.loader.Provider;
import ru.iis.probe.loader.DomXmlLoader;
import ru.iis.probe.validation.DepartmentBucketValidator;
import ru.iis.probe.writer.PgSqlWriter;
import ru.iis.probe.writer.Writer;

public class XmlToDatabaseProcess implements Process {
    private final String fileName;

    XmlToDatabaseProcess(String fileName){
        this.fileName = fileName;
    }

    @Override
    public void execute() {
        ConnectionPropertiesProvider connectionPropertiesProvider = new ConnectionPropertiesProvider("connection.properties");
        ConnectService connectService = new ConnectService(connectionPropertiesProvider);
        DataTransport<DepartmentsBucket> transport = new DataTransport<>();
        Provider<DepartmentsBucket> xmlProvider= new DomXmlLoader(fileName);
        Writer<DepartmentsBucket> dbWriter = new PgSqlWriter(connectService);
        transport.setValidator(new DepartmentBucketValidator());
        transport.send(xmlProvider, dbWriter);
    }
}
