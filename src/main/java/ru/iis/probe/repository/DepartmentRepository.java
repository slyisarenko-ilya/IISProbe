package ru.iis.probe.repository;

import ru.iis.probe.entity.Department;
import java.util.List;

public interface DepartmentRepository {

    void update(Department department);

    void insert(Department department);

    void delete(Department department);

    List<Department> queryAll();
}
