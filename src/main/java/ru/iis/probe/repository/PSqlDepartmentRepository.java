package ru.iis.probe.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iis.probe.entity.Department;
import ru.iis.probe.exception.IISProbeException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PSqlDepartmentRepository implements DepartmentRepository{
    private static final Logger logger = LoggerFactory.getLogger(PSqlDepartmentRepository.class);

    private final Connection conn;
    private final static String selectAllDepartmentsSql = "select id, DepCode, DepJob, Description from department";

    private final static String insertDepartmentsSql = "insert into department(DepCode, DepJob, Description) values (?, ?, ?)";
    private final static String updateDepartmentsSql = "update  department set Description = ? where depCode = ? and depJob = ?";
    private final static String deleteDepartmentsSql = "delete from department where DepCode = ? and DepJob = ?";


    public PSqlDepartmentRepository(Connection conn){
        this.conn = conn;
    }

    @Override
    public void  update(Department department){
        try(PreparedStatement updateStmt = conn.prepareStatement(updateDepartmentsSql)) {
            updateStmt.setString(1, department.getDescription());
            updateStmt.setString(2, department.getDepCode());
            updateStmt.setString(3, department.getDepJob());
            updateStmt.executeUpdate();
        } catch (SQLException e){
            try {
                if(!conn.getAutoCommit()) {
                    conn.rollback();
                }
            } catch(SQLException sqle){
                logger.error(sqle.getMessage());
            }
            logger.error(e.getMessage());
            throw new IISProbeException(e);
        }
    }

    @Override
    public void insert(Department department) {

        try (PreparedStatement insertStmt = conn.prepareStatement(insertDepartmentsSql)){
            insertStmt.setString(1, department.getDepCode());
            insertStmt.setString(2, department.getDepJob());
            insertStmt.setString(3, department.getDescription());
            insertStmt.executeUpdate();
        } catch (SQLException e){
            try {
                if(!conn.getAutoCommit()) {
                    conn.rollback();
                }
            } catch(SQLException sqle){
                logger.error(sqle.getMessage());
            }
            logger.error(e.getMessage());
            throw new IISProbeException(e);
        }
    }

    @Override
    public  void delete(Department department) {
        try(PreparedStatement deleteStmt = conn.prepareStatement(deleteDepartmentsSql)) {
            deleteStmt.setString(1, department.getDepCode());
            deleteStmt.setString(2, department.getDepJob());
            deleteStmt.executeUpdate();
        } catch (SQLException e){
            try {
                if(!conn.getAutoCommit()) {
                    conn.rollback();
                }
            } catch(SQLException sqle){
                logger.error(sqle.getMessage());
            }
            logger.error(e.getMessage());
            throw new IISProbeException(e);
        }

    }

    @Override
    public List<Department> queryAll(){
        try(Statement selectAllStmt = conn.createStatement()) {
            List<Department> departments = new ArrayList<>();
                ResultSet rs = selectAllStmt.executeQuery(selectAllDepartmentsSql);
                while (rs.next()) {
                    Department dep = new Department(rs.getInt("id"),
                            rs.getString("depCode"),
                            rs.getString("depJob"),
                            rs.getString("description"));
                    departments.add(dep);
                }
                return departments;
        } catch (SQLException e){
            try {
                if(!conn.getAutoCommit()) {
                    conn.rollback();
                }
            } catch(SQLException sqle){
                logger.error(sqle.getMessage());
            }
            logger.error(e.getMessage());
            throw new IISProbeException(e);
        }

    }
}
