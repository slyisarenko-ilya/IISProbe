package ru.iis.probe.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iis.probe.entity.CompareStrategy;
import ru.iis.probe.entity.Department;
import ru.iis.probe.entity.DepartmentsBucket;
import ru.iis.probe.entity.NaturalKeyDepartmentCompareStrategy;
import ru.iis.probe.exception.IISProbeException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Проверяет чтобы не было повторяющихся записей(по натуральному ключу) в источнике данных
 */
public class DepartmentBucketValidator implements SourceValidator<DepartmentsBucket>{
    private static final Logger logger =  LoggerFactory.getLogger(DepartmentBucketValidator.class);

    @Override
    public void validate(DepartmentsBucket entity){
        logger.info("Validate entity " + entity);
        CompareStrategy<Department> savedStrategy = Department.getCompareStrategy();//теперь сравнение будет происходить по натуральному ключу
        Department.setCompareStrategy(new NaturalKeyDepartmentCompareStrategy());
        List<Department> data = entity.getData();
        boolean containsUnique = containsUnique(data);
        Department.setCompareStrategy(savedStrategy); //возвращаем compareStrategy в былое состояние
        if(!containsUnique){
            throw new IISProbeException("Не должно быть повторяющихся записей в источнике данных");
        }
    }


    private static <T> boolean containsUnique(List<T> list){
        Set<T> set = new HashSet<>();

        for (T t: list){
            if (!set.add(t))
                return false;
        }

        return true;
    }

}
