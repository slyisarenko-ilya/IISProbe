package ru.iis.probe.validation;

public interface SourceValidator<T> {
    void validate(T entity);
}
