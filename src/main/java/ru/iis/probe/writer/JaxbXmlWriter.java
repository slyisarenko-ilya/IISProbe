package ru.iis.probe.writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iis.probe.entity.DepartmentsBucket;
import ru.iis.probe.exception.IISProbeException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class JaxbXmlWriter implements Writer<DepartmentsBucket> {
    private static final Logger logger =  LoggerFactory.getLogger(JaxbXmlWriter.class);

    private final String fileName;

    public JaxbXmlWriter(String fileName){
        this.fileName = fileName;
    }

    public void write(DepartmentsBucket entity) {

        try {
            JAXBContext jc = JAXBContext.newInstance( DepartmentsBucket.class );
            Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            logger.info(String.format("Save to file %s entity %s", fileName, entity));

            File file = new File(fileName);
            m.marshal( entity, file );

        } catch (JAXBException e) {
            throw new IISProbeException(e);
        }
    }


}
