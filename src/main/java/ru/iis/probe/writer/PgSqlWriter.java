package ru.iis.probe.writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iis.probe.connection.ConnectService;
import ru.iis.probe.entity.DepartmentsBucket;
import ru.iis.probe.exception.IISProbeException;
import ru.iis.probe.entity.Department;
import ru.iis.probe.repository.DepartmentRepository;
import ru.iis.probe.repository.PSqlDepartmentRepository;
import ru.iis.probe.writer.dbsync.DepartmentsSyncronizationService;
import ru.iis.probe.writer.dbsync.OperationType;

import java.sql.*;
import java.util.*;

public class PgSqlWriter implements Writer<DepartmentsBucket> {

    private static final Logger logger = LoggerFactory.getLogger(PgSqlWriter.class);

    private final ConnectService connectService;

    public PgSqlWriter(ConnectService connectService) {
        this.connectService = connectService;
    }

    public void write(DepartmentsBucket entity) {
        DepartmentsSyncronizationService syncronizationService = new DepartmentsSyncronizationService();
        try (Connection conn = connectService.getConnection()) {
            conn.setAutoCommit(false);
            DepartmentRepository departmentRepository = new PSqlDepartmentRepository(conn);
            Map<Department, OperationType> plan = syncronizationService.makePlan(departmentRepository, entity.getData());

            plan.keySet().forEach(dep -> {
                OperationType op = plan.get(dep);
                if (op == OperationType.INSERT) {
                    logger.info("insert " + dep);
                    departmentRepository.insert(dep);
                } else if (op == OperationType.UPDATE) {
                    departmentRepository.update(dep);
                    logger.info("update " + dep);
                } else if (op == OperationType.REMOVE) {
                    departmentRepository.delete(dep);
                    logger.info("delete " + dep);
                }
            });
            conn.commit();
            conn.setAutoCommit(true);
        } catch(SQLException e){
            throw new IISProbeException(e);
        }
    }
}
