package ru.iis.probe.writer;

public interface Writer<T> {
    void write(T entity);
}
