package ru.iis.probe.writer.dbsync;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iis.probe.entity.Department;
import ru.iis.probe.repository.DepartmentRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Составляет план запросов по обновлению записей БД на основе поступивших данных (например, из Xml)
 * План представляет собой карту Department -> Операция(Insert, update, delete)
 * На основе этого плана будет произведено обновление хранилища данных.
 */
public class DepartmentsSyncronizationService {
    private static final Logger logger = LoggerFactory.getLogger(DepartmentsSyncronizationService.class);

    public Map<Department, OperationType> makePlan(DepartmentRepository repository, List<Department> dataFromXml){
        Map<Department, OperationType> plan = new HashMap<>();

        //все поля, которые есть в data, но которых не было в БД - вставить (для этого предварительно добавляю в план все поля из dataFromXml с операцией insert)

        dataFromXml.forEach(department->
                plan.put(department, OperationType.INSERT)
        );

        List<Department> departments = repository.queryAll();
        for(Department dep: departments) {

            if (dataFromXml.contains(dep)) {
                //если есть в data и все поля одинаковы - то пометить как SKIP
                plan.put(dep, OperationType.SKIP);
            } else {
                //если нет в data - то удалить
                plan.put(dep, OperationType.REMOVE);
            }


            // если есть совпадение в dataFromXml по depCode и depJob, но desc различны - обновить
            dataFromXml.forEach(entity -> {
                if (entity.equalByNaturalKey(dep) && !entity.getDescription().equals(dep.getDescription())) {
                    plan.remove(dep);
                    plan.put(entity, OperationType.UPDATE);
                }
            });
        }
        logger.info("synchonization plan : " + plan);
        return plan;
    }

}

