package ru.iis.probe.writer.dbsync;

public enum OperationType {
        INSERT,
        REMOVE,
        UPDATE,
        SKIP
}
