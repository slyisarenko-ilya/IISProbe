package ru.iis.probe.entity;

import org.junit.Assert;
import org.junit.Test;

public class DepartmentTest {

    @Test
    public void testCompareByNaturalKey(){
        Department d1 = new Department(1, "test", "test2", "test3");
        Department d2 = new Department(2, "test", "test2", "test5");
        Department d3 = new Department(3, "test1", "test2", "test5");
        Assert.assertTrue(d1.equalByNaturalKey(d2));
        Assert.assertFalse(d1.equalByNaturalKey(null));
        Assert.assertFalse(d1.equalByNaturalKey(d3));
    }
}
